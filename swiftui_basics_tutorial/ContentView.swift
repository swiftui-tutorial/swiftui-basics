//
//  ContentView.swift
//  swiftui_basics_tutorial
//
//  Created by Kitti Jarearnsuk on 11/9/2565 BE.
//

import SwiftUI

struct ContentView: View {
    @State private var isNight: Bool = false
    
    var body: some View {
        ZStack {
            
            BackgroudView(isNight: $isNight)
            VStack{
                CityTextView(cityName: "Cupertino, CA")
                
                MainWeatherStatusView(imageName: isNight ? "moon.stars.fill" : "cloud.sun.fill",
                                      temperature: 78)
                
                HStack(spacing: 20){
                    WeatherDayView(datOfWeek: "TUE",
                                   imageName: "cloud.sun.fill",
                                   temperture: 74)
                    WeatherDayView(datOfWeek: "WED",
                                   imageName: "sun.max.fill",
                                   temperture: 88)
                    WeatherDayView(datOfWeek: "THU",
                                   imageName: "wind.snow",
                                   temperture: 55)
                    WeatherDayView(datOfWeek: "FRI",
                                   imageName: "sunset.fill",
                                   temperture: 60)
                    WeatherDayView(datOfWeek: "SAT",
                                   imageName: "snow",
                                   temperture: 25)
                    
                }
                
                Spacer()
                
                Button {
                    isNight.toggle()
                    print(isNight)
                } label: {
                    WeatherButton(title: "Change Day Time",
                                  textColor: .blue,
                                  backgroudColor: .white)
                }
                
                Spacer()
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

struct WeatherDayView: View {
    var datOfWeek: String
    var imageName: String
    var temperture: Int
    
    var body: some View {
        VStack{
            Text(datOfWeek)
                .font(.system(size: 16, weight: .medium, design: .default))
                .foregroundColor(.white)
            
            Image(systemName: imageName)
                .renderingMode(.original)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 40, height: 40)
            
            Text("\(temperture)°")
                .font(.system(size: 28,weight: .medium))
                .foregroundColor(.white)
        }
    }
}

struct BackgroudView: View {
     
    @Binding var isNight: Bool
    
    var body: some View {
        LinearGradient(gradient: Gradient(colors: [ isNight ? .black : .blue,
                                                    isNight ? .gray : Color("lightBlue")]),
                       startPoint: .topLeading,
                       endPoint: .bottomTrailing)
        .edgesIgnoringSafeArea(.all)
    }
}

struct CityTextView: View {
    var cityName: String
    var body: some View {
        Text(cityName)
            .font(.system(size: 32, weight: .medium, design: .default))
            .foregroundColor(.white)
            .padding()
    }
}

struct MainWeatherStatusView: View {
    var imageName: String
    var temperature: Int
    var body: some View {
        VStack(spacing: 10) {
            Image(systemName: imageName)
                .renderingMode(.original)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 180, height: 180)
            
            Text("\(temperature)°")
                .font(.system(size: 70,weight: .medium))
                .foregroundColor(.white)
        }
        .padding(.bottom,40)
    }
}


