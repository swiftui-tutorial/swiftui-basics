//
//  WeatherButton.swift
//  swiftui_basics_tutorial
//
//  Created by Kitti Jarearnsuk on 11/9/2565 BE.
//
import SwiftUI

struct WeatherButton: View {
    var title: String
    var textColor: Color
    var backgroudColor: Color
    
    var body: some View {
        Text("Change Day Time")
            .frame(width: 280, height: 60)
            .background(Color.white)
            .foregroundColor(.blue)
            .font(.system(size: 20, weight: .bold, design: .default))
            .cornerRadius(10)
    }
}
