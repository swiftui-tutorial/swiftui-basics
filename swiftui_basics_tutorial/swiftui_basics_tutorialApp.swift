//
//  swiftui_basics_tutorialApp.swift
//  swiftui_basics_tutorial
//
//  Created by Kitti Jarearnsuk on 11/9/2565 BE.
//

import SwiftUI

@main
struct swiftui_basics_tutorialApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
